<!DOCTYPE html>
<html>
<head>
	<title>index.php Search</title>	
	<link rel="stylesheet" type="text/css" href="<?php echo LOCALHOST; ?>/publics/css/mystyle">
</head>
<body>
		<!-- Search -->

		<div class="row" style="padding-top: 25px">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="container">
					<form style="margin-right:35%">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search" name="search">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
							</div>
						</div>
						<div class="btn-group btn-group-justified">
							<div class="btn-group">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter
								<span class="caret"></span></button>
								<ul class="dropdown-menu">
									<li><a href="#">by PRICE (up)</a></li>
									<li><a href="#">by PRICE (down)</a></li>
									<li><a href="#">by ALPHABETICAL ORDER</a></li>
								</ul>
							</div>
							<a href="#" class="btn btn-primary">Featured</a>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
		<!-- Products -->

				<div class="container" style="padding-top: 25px">
					<div class="row">
		<?php
			$conn = mysqli_connect("localhost","root","", "web_database");
			if (!$conn) {
			    die("Connection failed: " . mysqli_connect_error());
			}
			$sql = "SELECT * FROM products";
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
			    // output data of each row
			    while($row = $result->fetch_assoc()) {
			    	?>
						<div class="col-md-6" style="padding-bottom: 4%; padding-left: 5%">	
							<div class="col-md-4"><?php echo '<img src="data:image/png;base64,'.base64_encode( $row['image'] ).'"alt = "damn" style="width:150px;height:150px;">'; ?></a></div>
							<div class="col-md-2">
								<?php $this_id = $row["id"]; ?>
								<h2> <a href="index.php?controllers=main&method=swap&file=Product.php&id=<?php echo $this_id; ?>"><?php echo $row["name"];?></a></h2>	
								<p>PRICE: $<?php echo $row["price"];?></p>
							</div>
							<br>
						</div>

						<?php
		    }
		}
		?>
							
			</div>
			<br/>
		</div>



		
	</div>

</body>
</html>